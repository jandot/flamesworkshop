# FLAMES workshop Data Visualization KU Leuven

## Processing exercise

### Tutorial and instructions

To regenerate `processing_exercise.pdf`, run `make`.

### Datasets

* *flights.json* - Global Flights Network from [Visualising Global Marathon 2012 - Challenge 2](http://visualising.org/contests/visualizing-global-marathon-2012-challenge-2)
* *films.json* - Most profitable holywood stories from [Information Is Beautiful Awards](http://www.informationisbeautifulawards.com/2012/02/hollywood-dataviz-challenge-interactive-shortlist/). Data downloaded from [here](https://docs.google.com/spreadsheet/ccc?key=0Altk3Tn01ZsWdEJ1cHFjbmVyejhnN1JnQlNyWW5IUkE&authkey=CPuZiLcG#gid=29).
