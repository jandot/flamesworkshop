//// Static version
//Table table = loadTable("flights.csv","header");
//
//size(800,800);
//noStroke();
//colorMode(HSB,100);
//
//background(0,0,100); // set background to white
//for ( TableRow row : table.rows() ) {
//    float from_long = row.getFloat("from_long");
//    float from_lat = row.getFloat("from_lat");
//    String from_country = row.getString("from_country");
//    String to_country = row.getString("to_country");
//    int distance = row.getInt("distance");
//    
//    float x = map(from_long,-180,180,10,width-10);
//    float y = map(from_lat,-180,180,height-10,10);
//    if ( from_country.equals(to_country) ) {
//      fill(0,100,100,10);
//    } else {
//      fill(60,100,100,10);
//    }
//    float r = map(distance, 1, 15406, 3, 15);
//    
//    ellipse(x,y,r,r);
//}

//// Dynamic version 1
//Table table;
//
//void setup() {
//  size(800,800);
//  table = loadTable("flights.csv","header");
//  noLoop();
//  noStroke();
//  colorMode(HSB,100);
//}
//
//void draw() {
//  background(0,0,100); // set background to white
//  for ( TableRow row : table.rows() ) {
//    int distance = row.getInt("distance");
//    float mouseXMin = mouseX - 25;
//    float mouseXMax = mouseX + 25;
//    float minDistance = map(mouseXMin, -25, 810, 0, 15406);
//    float maxDistance = map(mouseXMax, -25, 810, 0, 15406);
//
//    if ( minDistance < distance && distance < maxDistance ) {
//      float from_long = row.getFloat("from_long");
//      float from_lat = row.getFloat("from_lat");
//      String from_country = row.getString("from_country");
//      String to_country = row.getString("to_country");
//  
//      float x = map(from_long,-180,180,10,width-10);
//      float y = map(from_lat,-180,180,height-10,10);
//      if ( from_country.equals(to_country) ) {
//        fill(0,100,100,10);
//      } else {
//        fill(60,100,100,10);
//      }
//      ellipse(x,y,3,3);
//    }
//  }
//}
//
//void mouseMoved() {
//  redraw();
//}

//// Dynamic version 2
//Table table;
//
//void setup() {
//  size(800,800);
//  table = loadTable("flights.csv","header");
//  noLoop();
//  strokeWeight(1);
//  colorMode(HSB,100);
//}
//
//void draw() {
//  background(0,0,100); // set background to white
//  for ( TableRow row : table.rows() ) {
//    int distance = row.getInt("distance");
//    float mouseXMin = mouseX - 25;
//    float mouseXMax = mouseX + 25;
//    float minDistance = map(mouseXMin, -25, 810, 0, 15406);
//    float maxDistance = map(mouseXMax, -25, 810, 0, 15406);
//
//    if ( minDistance < distance && distance < maxDistance ) {
//      float from_long = row.getFloat("from_long");
//      float from_lat = row.getFloat("from_lat");
//      float to_long = row.getFloat("to_long");
//      float to_lat = row.getFloat("to_lat");
//      String from_country = row.getString("from_country");
//      String to_country = row.getString("to_country");
//  
//      float from_x = map(from_long,-180,180,10,width-10);
//      float from_y = map(from_lat,-180,180,height-10,10);
//      float to_x = map(to_long,-180,180,10,width-10);
//      float to_y = map(to_lat,-180,180,height-10,10);
//      if ( from_country.equals(to_country) ) {
//        stroke(0,100,100,10);
//      } else {
//        stroke(60,100,100,10);
//      }
//      line(from_x,from_y,to_x,to_y);
//    }
//  }
//}
//
//void mouseMoved() {
//  redraw();
//}

// Dynamic version 1
Table table;

void setup() {
  size(800,800);
  table = loadTable("flights.csv","header");
  noLoop();
  colorMode(HSB,100);
}

void draw() {
  background(0,0,100); // set background to white
  for ( TableRow row : table.rows() ) {
    int distance = row.getInt("distance");
    float mouseXMin = mouseX - 25;
    float mouseXMax = mouseX + 25;
    float minDistance = map(mouseXMin, 0, 800, 0, 15406);
    float maxDistance = map(mouseXMax, 0, 800, 0, 15406);

    if ( minDistance < distance && distance < maxDistance ) {
      float from_long = row.getFloat("from_long");
      float from_lat = row.getFloat("from_lat");
      float to_long = row.getFloat("to_long");
      float to_lat = row.getFloat("to_lat");
      String from_country = row.getString("from_country");
      String to_country = row.getString("to_country");
  
      float from_x = map(from_long,-180,180,10,width-10);
      float from_y = map(from_lat,-180,180,height-10,10);
      float to_x = map(to_long,-180,180,10,width-10);
      float to_y = map(to_lat,-180,180,height-10,10);

      if ( from_country.equals(to_country) ) {
        fill(0,100,100,10);
      } else {
        fill(60,100,100,10);
      }
      ellipse(from_x,from_y-180,3,3);

      if ( from_country.equals(to_country) ) {
        stroke(0,100,100,10);
      } else {
        stroke(60,100,100,10);
      }
      line(from_x,from_y+180,to_x,to_y+180);
      noStroke();
    }
  }
}

void mouseMoved() {
  redraw();
}

